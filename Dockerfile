FROM python:3

ENV PYTHONBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN pip install pipenv
RUN pipenv install && pipenv install --system --deploy

EXPOSE 8000
ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8000", "{{ project_name}}.wsgi:application"]
