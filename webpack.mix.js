let mix = require('laravel-mix')

mix.js('{{ project_name }}/assets/js/app.js', '{{ project_name }}/static/js/app.js')
    .sass('{{ project_name }}/assets/sass/app.scss', '{{ project_name }}/static/css/app.css')
